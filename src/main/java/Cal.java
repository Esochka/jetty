public class Cal {

    public static double functionSum(double a, double b) {
        return a + b;
    }

    public static double functionDif(double a, double b) {
        return a - b;
    }

    public static double functionMul(double a, double b) {
        return a * b;
    }

    public static double functionDiv(double a, double b) {
        return a / b;
    }
}
