import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class Main {

    public static Double a = 0d;
    public static Double b = 0d;
    public static String op = " ";
    public static Double res = 0d;

    public static Double getRes() {
        return res;
    }

    public static void setRes(Double res) {
        Main.res = res;
    }

    public static Double getA() {
        return a;
    }

    public static void setA(Double a) {
        Main.a = a;
    }

    public static Double getB() {
        return b;
    }

    public static void setB(Double b) {
        Main.b = b;
    }

    public static String getOp() {
        return op;
    }

    public static void setOp(String op) {
        Main.op = op;
    }

    public static void main(String[] args) {

        int port = 8080;

        Server server = new Server(port);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        context.addServlet(new ServletHolder( new SrvltCalculator( ) ),"/calculate");
        context.addServlet(new ServletHolder( new SrvltHistiry( ) ),"/hist");
        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[] { context });
        server.setHandler(handlers);

        try {
            server.start();
            System.out.println("Listening port : " + port );
            server.join();
        } catch (Exception e) {
            System.out.println("Error.");
            e.printStackTrace();
        }

    }

}
