
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SrvltCalculator")
public class SrvltCalculator extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String stra = request.getParameter("a");
        String strb = request.getParameter("b");
        String strop = request.getParameter("op");

        double a = 0;
        double b = 0;
        double result = 0;


        try {

            a = Double.parseDouble(stra);
            b = Double.parseDouble(strb);

            if (strop.equals("+")){

                result = Cal.functionSum(a, b);
                Main.setA(a);
                Main.setB(b);
                Main.setOp("1");
                Main.setRes(result);
                doSetResult(response, result);
            }
            else if (strop.equals("-")){
                result = Cal.functionDif(a, b);
                Main.setA(a);
                Main.setB(b);
                Main.setOp("2");
                Main.setRes(result);
                doSetResult(response, result);
            }
            else if (strop.equals("*")){
                result = Cal.functionMul(a, b);
                Main.setA(a);
                Main.setB(b);
                Main.setOp("3");
                Main.setRes(result);
                doSetResult(response, result);
            }
            else if (strop.equals("/") && (b != 0)) {
                result = Cal.functionDiv(a, b);
                Main.setA(a);
                Main.setB(b);
                Main.setOp("4");
                Main.setRes(result);
                doSetResult(response, result);

            } else{
                Main.setA(0.0);
                Main.setB(0.0);
                Main.setOp("error");
                Main.setRes(0.0);
                doSetError(response);
            }


        } catch (Exception ex) {
            doSetError(response);
        }


    }

    protected void doSetResult(HttpServletResponse response, double result) throws UnsupportedEncodingException, IOException {

        String reply = "{\"error\":0,\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doSetError(HttpServletResponse response) throws UnsupportedEncodingException, IOException {
        String reply = "{\"error\":1}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_BAD_GATEWAY);
    }


}
