import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SrvltHistiry")
public class SrvltHistiry extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        try {
            if (!Main.getOp().startsWith("er") && !Main.getOp().startsWith(" ")) {
                System.out.println(Main.getB());
                System.out.println(Main.getA());
                System.out.println(Main.getOp());
                System.out.println(Main.getRes());
                doSetResult(response , Main.getB(), Main.getA() , Main.getOp(), Main.getRes());
                System.out.println("sdsds2");
            }

        } catch (Exception ex) {
            doSetError(response);
            System.out.println("sdsdddddddddddds");
        }


    }

    protected void doSetResult(HttpServletResponse response, double b, double a, String op, double result) throws UnsupportedEncodingException, IOException {

        String reply = "{\"error\":0,\"a\":" + Double.toString(a) + ",\"b\":" + Double.toString(b) + ",\"op\":" + op + ",\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doSetError(HttpServletResponse response) throws UnsupportedEncodingException, IOException {
        System.out.println("dsdsdsd");
        String reply = "{\"error\":1}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }


}
